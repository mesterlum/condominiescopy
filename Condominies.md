### Condominies

---
###### Register Condominie
* **POST** /api/v1/condominie/

Payload:

```json
    {
        "name": String,
        "address": String
    }
```

Reponse:

**400** -All parameters are required

return:

```json
    {
        "message": String
    }
```
**404** - User for admin condominie not found

return:

```json
    {
        "detail": String
    }
```

**201** - Condominie register

return:

```json
    {
        "name": String,
        "address": String,
        "status": String,
    }
```

---


###### Update Condominie
* **PUT** /api/v1/condominie/update

Payload:

```json
    {
        "name": String,     (optional)
        "address": String   (optional)
    }
```

responses:

**200** - Update condominie

return:

```json
    {
        "name": String,
        "address": String,
        "status": String,
    }
```

---


###### get info Condominie

responses:

**200** - Update condominie

return:

```json
    {
        "name": String,
        "address": String,
        "status": String,
    }
```

---