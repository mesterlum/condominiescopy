from django.urls import path, include
from rest_framework_jwt.views import obtain_jwt_token

urlpatterns = [
    path('users/', include('apps.users.urls'), name='users'),
    path('condominie/', include('apps.condominie.urls'), name='condominies'),
    path('departament/', include('apps.departament.urls'), name='departament'),
    path('events/', include('apps.events.urls'), name='events'),
    path('reports/', include('apps.reports.urls'), name='reports'),
    path('news/', include('apps.news.urls'), name='news'),
    path('services/', include('apps.services.urls'), name='services'),
    path('payments/', include('apps.payments.urls'), name='payments'),
    path('expenses/', include('apps.expenses.urls'), name='expenses'),
]