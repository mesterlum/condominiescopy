### News

---
###### Register new
* **POST** /api/v1/news/create/

Payload:

```json
    {
        "title": String,
        "description": String,
        "image": Image (Optional)
    }
```

Reponse:

**400** -All parameters are required

return:

```json
    {
        "message": String
    }
```

**500** - Internal error, Type image no valid

return:

```json
    {
        "message": String
    }
```

**201** - New created

return:

```json
    {
        "title": String,
        "description": String,
        "image_url": String,
        "created_at": Date
    }
```

###### Get List of the news
* **GET** /api/v1/news

return:

**200**

```json
    [
        {
            "title": String,
            "description": String,
            "image_url": String,
            "created_at": Date           
        }
    ]

```


###### Update New
* **GET** /api/v1/news/update

payload:

```json
    {
        "id": Int,
        "title": String,        (Optional)
        "description": String,   (Optional)
        "status": String        (Optional)  (Accepted: A=Active, C=Cancel)
    }
```

**400** -All parameters are required

return:

```json
    {
        "message": String
    }
```

**404** - New not found

```json
    {
        "detail": String
    }

```

return:

**200**

```json
    [
        {
            "title": String,
            "description": String,
            "image_url": String,
            "created_at": Date           
        }
    ]

```
