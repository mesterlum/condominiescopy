Django==2.1.1
psycopg2-binary==2.7.6.1
djangorestframework==3.8.2
djangorestframework-jwt==1.11.0
whitenoise==3.2
gunicorn==19.9.0
django-cors-headers==2.4.0
Pillow==5.2.0