### Events

---
###### Register Event
* **POST** /api/v1/events/

Payload:

```json
    {
        "title": String,
        "description": String,
        "date": String  ---Example: 2018-11-15T17:32:06.222865Z
    }
```

Reponse:

**400** -All parameters are required

return:

```json
    {
        "message": String
    }
```

**201** - Event register

return:

```json
    {
        "title": String,
        "description": String,
        "date": Date,
        "created_at": Date
    }
```


###### Get Events
* **GET** /api/v1/events/list

Response:

**200**

return:

```json
    [
        {
            "id": Int,
            "title": String,
            "description": String,
            "date": Date,
            "created_at": Date
        }
    ]

```


---

###### Update event
* **PUT** /api/v1/events/
Payload:

```json
    {
        "id": Int,
        "title": String,                                            (Optional)
        "description": String,                                      (Optional)
        "date": String,  ---Example: 2018-11-15T17:32:06.222865Z     (Optional)
        "status": String                                            (Optional)  (Accepted: A=Active, C=Cancel)
    }
```

Reponse:

**400** -All parameters are required

return:

```json
    {
        "message": String
    }
```

**404** - Event not found

return:

```json
    {
        "detail": String
    }
```

**200**

return:

```json
    [
        {
            "id": Int,
            "title": String,
            "description": String,
            "date": Date,
            "created_at": Date
        }
    ]

```