#### Users

---
###### LogIn
* **POST** /api/v1/users/login

Payload:

```json

    {
        "email": String,
        "password": String
    }

```

* Reponse:

**400** - Error

**200** - Login Sucess
return:

```json
    {
        "token": String
    }
```
---
---
###### Register
* **POST** /api/v1/users/register/

Payload:

```json
    {
        "email": String,
        "first_name": String,
        "last_name": String,
        "phone": String
    }
```
Response: 

**400** - All parameters are required

return:

```json
    {
        "message": String
    }
```
**409** - This email is already exists

return:

```json
    {
        "message": String
    }
```

**201** - Account created

return:

```json
    {
        "password": String,
        "email": String,
        "first_name": String,
        "last_name": String,
        "password": String
    }
```

---
###### Update profile user
* **PUT** /api/v1/users/

payload:

```json
    {
        "email": String,         (Optional)
        "first_name": String,    (Optional)
        "last_name": String,     (Optional)
        "phone": String,         (Optional)
        
    }
    
```

**200** - Update Profile Sueccess

return:

```json
    {   
        "id": Int,
        "password": String,     
        "email": String,        
        "first_name": String,   
        "last_name": String     
    }
    
```

###### Get user detail
* **GET** /api/v1/users/detail

return:

```json
    {
        "id": Int, 
        "email": String,        
        "first_name": String,   
        "last_name": String,
        "condominie": {
            "name": String,
            "address": String,
            "status": String
        }   
    }

```

---

---
###### Update or change password
* **PUT** /api/v1/users/change-password

payload:

```json
    {
        "new_password": String
    }
```

Responses:

**400** - All params requerid

```json
    {
        "message": String
    }

```

**200** - Change password Sucess

```json
    {
        "message": String
    }
```

---

###### Get users without departament
* **PUT** /api/v1/users/without-departament

response:

```json
[
    {   
        "id": Int,    
        "email": String,        
        "first_name": String,   
        "last_name": String     
    }
]
```