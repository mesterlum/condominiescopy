### Payments

---
###### Register Payment
* **POST** /api/v1/payments/

payload:

```json
    {
        "departament": Int,
        "service": Int
    }
```

responses:

**400** -All parameters are required

return:

```json
    {
        "message": String
    }
```

**404** - Departament not found or service not found

```json
    {
        "message": String
    }

```

**406** - Departament is free

```json
    {
        "message": String
    }
```

**201** - Payment Created

```json
    {
      "departament": {
            "number": String,
            "status": String,
            "date": Date
        },
        "service": {
            "service": String,
            "price": Int,
            "date": Date
        },
        "date": Date,
        "type_payment": String,
        "status": String
    }

```
---
###### Get List all departaments
* **GET** /api/v1/payments/

response:
```json
[
    {
      "departament": {
            "number": String,
            "status": String,
            "date": Date
        },
        "service": {
            "service": String,
            "price": Int,
            "date": Date
        },
        "date": Date,
        "type_payment": String,
        "status": String
    }
]
```


###### Get departament debts
* **GET** /api/v1/payments/**id_departament**

response:
```json
[
    {
        "service": {
            "service": String,
            "price": Int,
            "date": Date
        },
        "date": Date,
        "type_payment": String,
        "status": String
    }
]
```

###### Make payment from admin
* **PUT** /api/v1/payments/make-payment

Payload:

```json
    {
        "id": Int
    }
```

Responses:

**400** -All parameters are required

return:

```json
    {
        "message": String
    }
```

**404** - Payment not foun

```json
    {
        "message": String
    }

```

**200** - Status changed

return:

```json
    {
        "message": String
    }
```


###### Register Payment from user
* **POST** /api/v1/payments/user

payload:

```json
    {
        "service": Int
    }
```

responses:

**400** -All parameters are required

return:

```json
    {
        "message": String
    }
```

**404** - Departament not found or service not found

```json
    {
        "message": String
    }

```

**201** - Payment Created

```json
    {
      "departament": {
            "number": String,
            "status": String,
            "date": Date
        },
        "service": {
            "service": String,
            "price": Int,
            "date": Date
        },
        "date": Date,
        "type_payment": String,
        "status": String
    }

```

###### cancel payment (Admin)
* **PUT** /api/v1/payments/cancel

payload:

```json
    {
        "id": Int
    }
```

responses:

**400** -All parameters are required

return:

```json
    {
        "message": String
    }
```

**404** - Payment not found

```json
    {
        "message": String
    }

```

**200** - Payment change status

```json
    {
        "message": String
    }
```


###### Get departament list debts
* **GET** /api/v1/payments/user/list

response:
```json
[
    {
        "service": {
            "service": String,
            "price": Int,
            "date": Date
        },
        "date": Date,
        "type_payment": String,
        "status": String
    }
]
```

###### Get info debts from id departamnt
* **GET** /api/v1/payments/departament/  **id_departament**

**404** - Departament not found

```json
    {
        "detail": String
    }
```

**200** - list Debts
response:
```json
[
    {
        "service": {
            "service": String,
            "price": Int,
            "date": Date
        },
        "date": Date,
        "type_payment": String,
        "status": String
    }
]
```



