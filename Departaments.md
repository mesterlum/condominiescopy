### Departament

---
###### Register Departament
* **POST** /api/v1/departament/

Payload:

```json
    {
        "number": String
    }
```

Reponse:

**400** -All parameters are required

return:

```json
    {
        "message": String
    }
```

**201** - departament register

return:

```json
    {
        "number": String,
        "status": String,
        "date": Date
    }
```

###### List Departaments
* **GET** /api/v1/departament/

Return:

```json
    [
        {
            "number": String,
            "status": String,
            "date": Date
        }
    ]
```

###### List Departaments Availables
* **GET** /api/v1/departament/availables

Return:

```json
    [
        {
            "number": String,
            "status": String,
            "date": Date,
            "reports": {    // if exists this property is applied
                "description": String,
                "status": String,
                "date": Date
            }
        }
    ]
```

###### Assign User in departament
* **POST** /api/v1/departament/assign

Payload:

```json
    {
        "departament": Int,
        "user": Int
    }
```

Response:

**400** -All parameters are required

return:

```json
    {
        "message": String
    }
```

**404** - Departament or User not found

return: 

```json
    {
        "detail": String
    }

```
**302** - User exists in departament

return:

```json
    {
        "message": String
    }

```

**201** - User assiged in the departament

return

```json
[
    {
        "id": Int,
        "departament":{
            "number": String,
            "status": String,
            "date": Date
        },
        "user": {
            "id": Int,
            "email": String,
            "first_name": String,
            "last_name": String,
            "phone": String
        }
    }
]
```


###### Check my tenants
* **GET** /api/v1/departament/tenants

return:

```json
[
    {
        "id": Int,
        "departament":{
            "number": String,
            "status": String,
            "date": Date
        },
        "user": {
            "id": Int,
            "email": String,
            "first_name": String,
            "last_name": String,
            "phone": String
        }
    }
]
```

###### Get Departament by id
* **GET** /api/v1/departament/**id**

return:

```json

    {
        "number": String,
        "status": String,
        "date": Date
    }

```

###### Check who lives in the departaments
* **GET** /api/v1/departament/check/**id**

return:

```json
[
    {
        "id": Int,
        "departament":{
            "number": String,
            "status": String,
            "date": Date
        },
        "user": {
            "id": Int,
            "email": String,
            "first_name": String,
            "last_name": String,
            "phone": String
        }
    }
]
```