## ToDo List API

###### Users
- [x] Register
- [x] LogIn
- [x] Update Profile
- [x] Get User detail
- [x] Check user is assigned in departament

###### Condominie
- [x] Register Condominie
- [x] Update info condominie
- [x] Get Info condominie

###### Departament
- [x] Register departament
- [x] Assign user for departament
- [x] Get departament by id
- [x] Check my tenants
- [x] Get departaments availables
- [x] Get list departaments
- [x] Check who lives in the departament


###### Services
- [x] Register service
- [x] Get Services
- [x] Update service
- [x] Get services for users

###### Events
- [x] Register Event
- [x] Get all events
- [x] Update event

###### Reports
- [x] Register Report
- [x] Filter Reports
- [x] Update Status report
- [x] Update data of the report
- [x] Check report
- [x] List reports of the clients
- [x] Change status as pending

###### News
- [x] Register New
- [x] Upload Image
- [x] Get News (LIST)
- [x] Get Image
- [x] Update new

###### Payments
- [x] Make a payment (Change status) Admin
- [x] Get user balance
- [x] create register for pay
- [x] Get list departamnets with debts
- [x] Make a payment from User
- [x] Cancel payment
- [x] Get debts from User
- [x] Get debts with Id departament
- [] View user
- [] View admin

###### Expenses
- [x] Register Expense
- [x] Get expenses
- [x] Filter expenses
- [x] Update expense



## EndPoints

---

### Users
- Users.md

### Condominies
- Codnominies.md

### Reports
- Reports.md

### Events
- Events.md

### News
- News.md

### Departaments
- Departaments.md

### Payments
- Payments.md

### Expenses
- Expenses.md




