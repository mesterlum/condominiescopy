### Services

---
###### Register service
* **POST** /api/v1/services/

Payload:

```json
    {
        "service": String,
        "price": Int
    }
```

Responses:

**400** - All parameters are required

return:

```json
    {
        "message": String
    }
```

**201** - Service Created

return:

```json
    {
        "id": Int,
        "service": String,
        "price": Int,
        "created_at": Date
    }
```

###### Update service
* **PUT** /api/v1/services/

payload:

```json
    {
        "id": Int,
        "price": Int,       (Optional)
        "service": String,   (Optional)
        "visibility": String (Optional)         (O = 'All', A=Admin)
    }

```

responses:

**400** - All parameters are required

return:

```json
    {
        "message": String
    }
```

**404** - Not found service

return:

```json
    {
        "detail": String
    }
```

**200** - Service update

return:

```json
    {
        "id": Int,
        "service": String,
        "price": Int,
        "created_at": Date
    }
```

###### get all services
* **GET** /api/v1/services/

responses:

**200**

return:
```json
[
    {
        "id": Int,
        "service": String,
        "price": Int,
        "created_at": Date
    }
]
```

###### get all services for users
* **GET** /api/v1/services/user

responses:

**200**

return:
```json
[
    {
        "id": Int,
        "service": String,
        "price": Int,
        "created_at": Date
    }
]
```