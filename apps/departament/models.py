from django.db import models

# extern Models
from apps.users.models import Users
from apps.condominie.models import Condominie

class Departament(models.Model):
    
    STATUS_CHOICES = (
        ('A', 'Available'),
        ('B', 'Busy'),
        ('M', 'Maintenance'),
    )
    condominie = models.ForeignKey(Condominie, on_delete=models.CASCADE)
    number = models.CharField(max_length=4, null=False)
    status = models.CharField(max_length=1, null=False, default=STATUS_CHOICES[0][0], choices=STATUS_CHOICES)
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.number


class DepartamentUser(models.Model):
    departament = models.ForeignKey(Departament, on_delete=models.CASCADE)
    user = models.ForeignKey(Users, on_delete=models.CASCADE)
    date_joined = models.DateTimeField(auto_now_add=True)

