from rest_framework.views import APIView
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response
from rest_framework import status
from rest_framework.generics import ListAPIView, RetrieveAPIView
from django.shortcuts import get_object_or_404
from django.db.models import Q
# Models
from .models import Departament, DepartamentUser
from apps.users.models import Users
from apps.reports.models import Report

# Serializers
from .api.serializers import DepartamentSerializer, DepartamentWithUserSerializer
from apps.reports.api.serializers import ReportSerializer

class DepartamentView(APIView):
    
    permission_classes = (IsAdminUser,)

    # Register Departament

    def post(self, request):
        if not 'number' in request.data:
            return Response({'message': 'Todos los campos son requeridos: number'}, status=status.HTTP_400_BAD_REQUEST)
            
        number = request.data['number']
        departament = Departament(number=number)
        departament.condominie = request.user.condominie
        departament.save()
        departament_serializer = DepartamentSerializer(departament)

        return Response(departament_serializer.data, status=status.HTTP_201_CREATED)


    # Get list departaments if exists report return 

    def get(self, request):
        departaments = Departament.objects.filter(condominie=request.user.condominie)
        context = []
        for departament in departaments:
            report = Report.objects.filter(Q(departament_report=departament) & (Q(status='U') | Q(status='P')))
            report_serializer = ReportSerializer(report, many=True)
            departament_serializer = DepartamentSerializer(departament)
            sub_context = departament_serializer.data
            if len(report_serializer.data) > 0:
                sub_context['reports'] = report_serializer.data
            context.append(sub_context)
            
        
        return Response(context)


class DepartamentsAvaiableView(ListAPIView):

    permission_classes = (IsAdminUser,)
    serializer_class = DepartamentSerializer

    def get_queryset(self):
        departaments = Departament.objects.filter(condominie=self.request.user.condominie, status='A')
        return departaments

class AssignUserInDepartamentView(APIView):

    permission_classes = (IsAdminUser,)

    def post(self, request):
        if not 'departament' in request.data or not 'user' in request.data:
            return Response({'message': 'Todos los campos son requeridos: departament, user'}, status=status.HTTP_400_BAD_REQUEST)

        departament = get_object_or_404(Departament, id=request.data['departament'])

        if departament.status != 'A':
            return Response({'message': 'Este departamento no esta disponible'})

        user = get_object_or_404(Users, id=request.data['user'])
        
        try:
            DepartamentUser.objects.get(user=user)
            return Response({'message': 'Este usuario ya ocupa un departamento'}, status=status.HTTP_302_FOUND)
        except DepartamentUser.DoesNotExist:
            pass

        departament_user = DepartamentUser(user=user, departament=departament)
        departament.status = 'B' # Change Status for BUSY
        departament.save()
        departament_user.save()
        departament_user_serializer = DepartamentWithUserSerializer(departament_user)
        
        return Response(departament_user_serializer.data, status=status.HTTP_201_CREATED)

class CheckTenantsView(ListAPIView):

    permission_classes = (IsAdminUser,)
    serializer_class = DepartamentWithUserSerializer

    def get_queryset(self):
        tenants = DepartamentUser.objects.filter(departament__condominie=self.request.user.condominie)
        return tenants

class RetrieveDepartamentView(RetrieveAPIView):
    
    lookup_field = 'pk'
    permission_classes = (IsAdminUser,)
    serializer_class = DepartamentSerializer

    def get_queryset(self):
        return Departament.objects.filter(id=self.kwargs['pk'],
                                              condominie=self.request.user.condominie)



class RetrieveDepartamentWithUserView(APIView):
    
    def get(self, request, pk):
        departament_user = get_object_or_404(DepartamentUser, departament__id=pk,
                                              departament__condominie=request.user.condominie)

        departament_user_serializer = DepartamentWithUserSerializer(departament_user)

        return Response(departament_user_serializer.data)

        

        





