from rest_framework import serializers

# Models
from ..models import Departament, DepartamentUser

# Serializers
from apps.users.api.serializers import UsersSerializer

class DepartamentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Departament
        fields = ('id', 'number', 'status', 'date')

class DepartamentWithUserSerializer(serializers.ModelSerializer):
    departament = DepartamentSerializer()
    user = UsersSerializer()
    class Meta:
        model = DepartamentUser
        fields = ('id', 'departament', 'user', 'date_joined')