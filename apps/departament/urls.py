from django.urls import path

from . import views

urlpatterns = [
    path('', views.DepartamentView.as_view()),
    path('<int:pk>', views.RetrieveDepartamentView.as_view()),
    path('availables', views.DepartamentsAvaiableView.as_view()),
    path('assign', views.AssignUserInDepartamentView.as_view()),
    path('tenants', views.CheckTenantsView.as_view()),
    path('check/<int:pk>', views.RetrieveDepartamentWithUserView.as_view()),
]