from rest_framework.views import APIView
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response
from rest_framework import status
from rest_framework.generics import ListAPIView
from django.shortcuts import get_object_or_404

# Models
from .models import Event

# Serializers
from .api.serializers import EventSerializer

class EventsView(APIView):
    
    permission_classes = (IsAdminUser,)

    # Register event
    def post(self, request):
        if not 'description' in request.data or not 'date' in request.data or not 'title' in request.data:
            return Response({'message': 'Todos los campos son requeridos: title, description, date'}, status=status.HTTP_400_BAD_REQUEST)
        
        title = request.data['title']
        description = request.data['description']
        date = request.data['date']

        event = Event(title=title, description=description, date=date, condominie=request.user.condominie)
        event.save()

        event_serializer = EventSerializer(event)

        return Response(event_serializer.data, status=status.HTTP_201_CREATED)

    # Update event
    def put(self, request):
        if not 'id' in request.data:
            return Response({'message': 'El id es requerido'}, status=status.HTTP_400_BAD_REQUEST)
        
        event = get_object_or_404(Event, id=request.data['id'])

        allowed_fields = ['description', 'date', 'title', 'status']

        for key in request.data:
            if key in allowed_fields:
                setattr(event, key, request.data[key])

        event.save()

        event_serializer = EventSerializer(event)

        return Response(event_serializer.data)

        


class EventsForUsersView(ListAPIView):

    serializer_class = EventSerializer

    def get_queryset(self):
        return Event.objects.filter(condominie=self.request.user.condominie)
        


