from django.db import models

# Models
from apps.condominie.models import Condominie

class Event(models.Model):

    STATUS_CHOICES = (
        ('A', 'Active'),
        ('C', 'Cancel'),
    )

    title = models.CharField(max_length=100, null=False)
    description = models.CharField(max_length=400, null=False)
    date = models.DateTimeField()
    created_at = models.DateTimeField(auto_now_add=True)
    condominie = models.ForeignKey(Condominie, on_delete=models.CASCADE)
    status = models.CharField(max_length=1, choices=STATUS_CHOICES, default=STATUS_CHOICES[0][0])

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['-date']
