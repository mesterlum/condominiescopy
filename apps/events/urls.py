from django.urls import path

from . import views

urlpatterns = [
    path('', views.EventsView.as_view()),
    path('list', views.EventsForUsersView.as_view()),
]