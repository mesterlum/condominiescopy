# Generated by Django 2.1.1 on 2018-11-20 03:00

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('departament', '0001_initial'),
        ('services', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('type_payment', models.CharField(choices=[('C', 'Cash'), ('M', 'Mercado pago')], max_length=1, null=True)),
                ('status', models.CharField(choices=[('PE', 'Pending'), ('PA', 'Paid')], default='PE', max_length=2)),
                ('departament', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='departament.Departament')),
                ('service', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='services.Service')),
            ],
        ),
    ]
