# Generated by Django 2.1.1 on 2018-11-22 04:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payments', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='payment',
            name='status',
            field=models.CharField(choices=[('PE', 'Pending'), ('PA', 'Paid Out')], default='PE', max_length=2),
        ),
    ]
