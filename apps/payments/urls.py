from django.urls import path

from . import views

urlpatterns = [
    path('', views.PaymentsView.as_view()),
    path('<int:departament>', views.PaymentsForDepartamentView.as_view()),
    path('make-payment', views.MakeAPaymentAdminView.as_view()),
    path('user', views.RegisterPaymentFromUserView.as_view()),
    path('cancel', views.CancelPaymentView.as_view()),
    path('user/list', views.GetDebtsUserView.as_view()),
    path('departament/<int:pk_departament>', views.ListDebtsForDepartament.as_view()),
    path('notify', views.NotifyPayView.as_view()),
]