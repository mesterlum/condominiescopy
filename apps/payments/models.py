from django.db import models

# Models
from apps.departament.models import Departament
from apps.services.models import Service

class Payment(models.Model):

    STATUS_CHOICES = (
        ('PE', 'Pending'),
        ('PA', 'Paid Out'),
        ('CA', 'Cancel'),
    )

    TYPE_PAYMENT_CHOICES = (
        ('C', 'Cash'),
        ('M', 'Mercado pago'),
    )

    departament = models.ForeignKey(Departament, on_delete=models.CASCADE)
    service = models.ForeignKey(Service, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)
    type_payment = models.CharField(max_length=1, choices=TYPE_PAYMENT_CHOICES, null=True)
    status = models.CharField(max_length=2, choices=STATUS_CHOICES, default=STATUS_CHOICES[0][0])

