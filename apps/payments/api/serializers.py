from rest_framework import serializers

# Models
from ..models import Payment

# Serializers
from apps.departament.api.serializers import DepartamentSerializer
from apps.services.api.serializers import ServiceSerializer

class PaymentSerializer(serializers.ModelSerializer):
    departament = DepartamentSerializer()
    service = ServiceSerializer()
    class Meta:
        model = Payment
        fields = ('id', 'departament', 'service', 'date', 'type_payment', 'status',)

class PaymentSerializerWithoutDepartamentDetail(serializers.ModelSerializer):
    service = ServiceSerializer()
    class Meta:
        model = Payment
        fields = ('id', 'service', 'date', 'type_payment', 'status',)
