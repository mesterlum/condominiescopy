from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAdminUser
from rest_framework.generics import ListAPIView
from django.shortcuts import get_object_or_404, get_list_or_404

# Models
from apps.departament.models import DepartamentUser, Departament
from apps.services.models import Service
from apps.users.models import Users
from .models import Payment

# Serializers
from .api.serializers import PaymentSerializer, PaymentSerializerWithoutDepartamentDetail

class PaymentsView(APIView):
    permission_classes = (IsAdminUser,)
    
    def post(self, request):
        if not 'departament' in request.data or not 'service' in request.data:
            return Response({'message': 'Todos los campos son requeridos: departament, service'}, status=status.HTTP_400_BAD_REQUEST)

        departament = request.data['departament']
        service = request.data['service']

        departament = get_object_or_404(Departament, id=departament)
        service = get_object_or_404(Service, id=service, condominie=request.user.condominie)
        
        # Verify exist user in departament
        verify_departament_is_busy = DepartamentUser.objects.filter(departament=departament)
        
        if len(verify_departament_is_busy) < 1:
            return Response({'message': 'Este departamento no esta ocupado, no se puede registrar un servicio aqui.'}, status=status.HTTP_406_NOT_ACCEPTABLE)
        
        # Clean variable
        del verify_departament_is_busy

        new_payment = Payment(departament=departament, service=service)
        new_payment.save()

        new_payment_serializer = PaymentSerializer(new_payment)

        return Response(new_payment_serializer.data, status=status.HTTP_201_CREATED)

    def get(self, request):
        all_payments = Payment.objects.filter(departament__condominie=request.user.condominie)
        payments_serializer = PaymentSerializer(all_payments, many=True)
        return Response(payments_serializer.data)

class PaymentsForDepartamentView(ListAPIView):
    serializer_class = PaymentSerializerWithoutDepartamentDetail
    permission_classes = (IsAdminUser,)
    lookup_field = 'departament'

    def get_queryset(self):
        return Payment.objects.filter(departament__id=self.kwargs['departament'],
                                    departament__condominie=self.request.user.condominie,
                                    status='PE')

class MakeAPaymentAdminView(APIView):

    permission_classes = (IsAdminUser,)

    def put(self, request):
        if not 'id' in request.data:
            return Response({'message': 'El id es necesario.'}, status=status.HTTP_200_OK)

        payment = get_object_or_404(Payment, id=request.data['id'])

        payment.status = 'PA' # Change status
        payment.save()

        return Response({'message': 'Ok!'})

class RegisterPaymentFromUserView(APIView):

    def post(self, request):
        if not 'service' in request.data:
            return Response({'message': 'Todos los campos son requeridos: service'}, status=status.HTTP_400_BAD_REQUEST)

        service = request.data['service']

        service = get_object_or_404(Service, id=service, condominie=request.user.condominie, visibility='O')
        
        # Get departament from user
        departament_user = get_object_or_404(DepartamentUser, user=request.user)

        new_payment = Payment(departament=departament_user.departament, service=service)
        new_payment.save()

        new_payment_serializer = PaymentSerializer(new_payment)
        
        return Response(new_payment_serializer.data, status=status.HTTP_201_CREATED)

class CancelPaymentView(APIView):

    permission_classes = (IsAdminUser,)

    def put(self, request):
        if not 'id' in request.data:
            return Response({'message': 'Todos los campos son requeridos: id'}, status=status.HTTP_400_BAD_REQUEST)

        payment = get_object_or_404(Payment, id=request.data['id'])

        payment.status = 'CA'
        payment.save()

        return Response({'message': 'Ok!'})

class GetDebtsUserView(ListAPIView):

    serializer_class = PaymentSerializerWithoutDepartamentDetail

    def get_queryset(self):
        # Get departament from user
        departament_user = get_object_or_404(DepartamentUser, user=self.request.user)

        return get_list_or_404(Payment, departament=departament_user.departament)

class ListDebtsForDepartament(ListAPIView):
    permission_classes = (IsAdminUser,)
    serializer_class = PaymentSerializerWithoutDepartamentDetail
    lookup_field = 'pk_departament'

    def get_queryset(self):
        return get_list_or_404(Payment, departament__id=self.kwargs['pk_departament'], 
                                departament__condominie=self.request.user.condominie)



# This method is for the TEST
class NotifyPayView(APIView):

    permission_classes = (AllowAny,)

    def get(self, request):
        from twilio.rest import Client
        from datetime import date
        account = "ACef021032c323d2ace90be97bffe3dfaa"
        token = "23da5845acb9f745bdb4eda523d395d2"
        client = Client(account, token)
        today = date.today()
        payments = Payment.objects.filter(status='PE', date__year=today.year)
        
        departaments_user = []
        for payment in payments:
            try:
                departaments_user.append(DepartamentUser.objects.get(departament=payment.departament))
            except DepartamentUser.DoesNotExist:
                pass
        
        for departament_user in departaments_user:
            message = client.messages.create(to='whatsapp:+{}'.format(departament_user.user.phone), from_='whatsapp:+14155238886',
                                        body='''
                                                Buenas dias/tardes/noches, *{first_name} {last_name}*, le agradecemos su preferencia en condominis camilla's
                                                por favor, pase a liquidar su cuenta, le agradecemos que pase a pagar lo mas pronto posible.
                                                Depas camilla's
                                            '''.format(first_name=departament_user.user.first_name, last_name=departament_user.user.last_name))

            print(message.sid)
            print('whatsapp:+{}'.format(departament_user.user.phone))


        return Response('Enviados')



