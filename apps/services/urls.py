from django.urls import path

from . import views

urlpatterns = [
    path('', views.ServicesView.as_view()),
    path('user', views.ServicesForUsersView.as_view()),
    
]