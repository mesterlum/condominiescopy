from django.db import models

# Models
from apps.condominie.models import Condominie

class Service(models.Model):

    VISIBILITY_CHOICES = (
        ('A', 'Only Admin'),
        ('O', 'Alls'),
    )

    service = models.CharField(max_length=100, null=False, unique=True)
    price = models.PositiveIntegerField()
    condominie = models.ForeignKey(Condominie, on_delete=models.CASCADE)
    visibility = models.CharField(max_length=1, choices=VISIBILITY_CHOICES, default=VISIBILITY_CHOICES[1][0])
    created_at = models.DateTimeField(auto_now_add=True)


    def __str__(self):
        return self.service
