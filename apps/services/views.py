from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from rest_framework.permissions import IsAdminUser
from django.shortcuts import get_object_or_404
from rest_framework.generics import ListAPIView

# Models
from .models import Service

# Serializers
from .api.serializers import ServiceSerializer

class ServicesView(APIView):
    permission_classes = (IsAdminUser,)

    def post(self, request):
        if not 'service' in request.data or not 'price' in request.data:
            return Response({'message': 'Todos los campos son requeridos: service, price'}, status=status.HTTP_400_BAD_REQUEST)
        
        service = request.data['service']
        price = request.data['price']

        service = Service(service=service, price=price, condominie=request.user.condominie)
        service.save()
        
        service_serializer = ServiceSerializer(service)
        return Response(service_serializer.data, status=status.HTTP_201_CREATED)

    def get(self, request):
        services = Service.objects.filter(condominie=request.user.condominie)
        service_serializer = ServiceSerializer(services, many=True)
        return Response(service_serializer.data)

    def put(self, request):
        if not 'id' in request.data:
            return Response({'message': 'El id es requerido'}, status=status.HTTP_400_BAD_REQUEST)

        service = get_object_or_404(Service, id=request.data['id'])

        allowed_fields = ['service', 'price', 'visibility']

        for key in request.data:
            if key in allowed_fields:
                setattr(service, key, request.data[key])

        service.save()

        service_serializer = ServiceSerializer(service)
        
        return Response(service_serializer.data)

class ServicesForUsersView(ListAPIView):
    
    serializer_class = ServiceSerializer

    def get_queryset(self):
        return Service.objects.filter(condominie=self.request.user.condominie, visibility='O')




