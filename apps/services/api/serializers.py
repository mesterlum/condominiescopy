from rest_framework import serializers

# Models
from ..models import Service

class ServiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Service
        fields = ('id', 'service', 'price', 'created_at')
