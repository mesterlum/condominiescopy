from django.urls import path

from . import views

urlpatterns = [
    path('', views.ExpensesView.as_view()),
    path('filter', views.ExpensesFilterView.as_view()),

]