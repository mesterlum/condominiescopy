# Generated by Django 2.1.1 on 2018-11-21 05:38

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('condominie', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Expense',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('concept', models.CharField(max_length=100)),
                ('description', models.CharField(max_length=300)),
                ('price', models.IntegerField()),
                ('status', models.CharField(choices=[('PE', 'Pending'), ('PA', 'Paid Out')], default='PE', max_length=2)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('condominie', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='condominie.Condominie')),
            ],
        ),
    ]
