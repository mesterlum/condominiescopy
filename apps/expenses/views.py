from rest_framework import status
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView
from django.shortcuts import get_object_or_404

# Models
from .models import Expense

# Serializer
from .api.serializers import ExpenseSerializer

class ExpensesView(APIView):
    
    permission_classes = (IsAdminUser,)

    # Register Expense
    def post(self, request):
        if not 'concept' in request.data or not 'description' in request.data or not 'price' in request.data:
            return Response({'message': 'Todos los parametros son requeridos: concept, description, price'}, status=status.HTTP_400_BAD_REQUEST)

        concept = request.data['concept']
        description = request.data['description']
        price = request.data['price']

        expense = Expense(concept=concept, description=description, price=price, condominie=request.user.condominie)
        expense.save()

        expense_serializer = ExpenseSerializer(expense)

        return Response(expense_serializer.data, status=status.HTTP_201_CREATED)

    # Get all expenses
    def get(self, request):
        expenses = Expense.objects.filter(condominie=request.user.condominie)
        
        expenses_serializer = ExpenseSerializer(expenses, many=True)

        return Response(expenses_serializer.data)

      
    # Update expense
    def put(self, request):
        if not 'id' in request.data:
            return Response({'message': 'el id es requerido'}, status=status.HTTP_400_BAD_REQUEST)

        expense = get_object_or_404(Expense, id=request.data['id'])

        allowed_fields = ['concept', 'description', 'price', 'status']

        for key in request.data:
            if key in allowed_fields:
                setattr(allowed_fields, key, request.data[key])

        expense.save()

        expense_serializer = ExpenseSerializer(expense)
        
        return Response(expense_serializer.data)
    

class ExpensesFilterView(ListAPIView):

    permission_classes = (IsAdminUser,)
    serializer_class = ExpenseSerializer

    def get_queryset(self):
        if not 'status' in self.request.GET:
            return Expense.objects.filter(condominie=self.request.user.condominie)
        
        status = self.request.GET['status']
        return Expense.objects.filter(condominie=self.request.user.condominie, status=status)
