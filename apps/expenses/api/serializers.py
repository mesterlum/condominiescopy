from rest_framework import serializers

# Model
from ..models import Expense

class ExpenseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Expense
        exclude = ('condominie',)