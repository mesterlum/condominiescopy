from django.db import models

# Models
from apps.condominie.models import Condominie

class Expense(models.Model):
    
    STATUS_CHOICES = (
        ('PE', 'Pending'),
        ('PA', 'Paid Out')
    )

    concept = models.CharField(max_length=100)
    description = models.CharField(max_length=300)
    price = models.IntegerField()
    condominie = models.ForeignKey(Condominie, on_delete=models.CASCADE)
    status = models.CharField(max_length=2, choices=STATUS_CHOICES, default=STATUS_CHOICES[1][0])
    created_at = models.DateTimeField(auto_now_add=True)


    def __str__(self):
        return self.concept
