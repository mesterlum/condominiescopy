from rest_framework import serializers

# Models
from ..models import New

class NewSerializer(serializers.ModelSerializer):
    image_url = serializers.ImageField(use_url=True, source="image", max_length=None)

    class Meta:
        model = New
        fields = ('id', 'title', 'description', 'image_url', 'created_at', 'status',)