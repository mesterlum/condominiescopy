from django.db import models

# Models
from apps.condominie.models import Condominie

class New(models.Model):

    STATUS_CHOICES = (
        ('A', 'Active'),
        ('C', 'Cancel'),
    )

    title = models.CharField(max_length=100, null=False)
    description = models.CharField(max_length=400, null=False)
    image = models.ImageField(upload_to='news/', null=True, blank=True, default='news/default.jpg')
    condominie = models.ForeignKey(Condominie, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)


    def __str__(self):
        return self.title


    class Meta:
        ordering = ['-created_at']
