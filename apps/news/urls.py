from django.urls import path

from . import views

urlpatterns = [
    path('', views.NewsView.as_view()),
    path('update', views.UpdateNew.as_view()),
    path('create', views.CreateNew.as_view()),
]