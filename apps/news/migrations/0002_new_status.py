# Generated by Django 2.1.1 on 2018-11-23 19:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='new',
            name='status',
            field=models.CharField(choices=[('A', 'Active'), ('C', 'Cancel')], default='A', max_length=1),
        ),
    ]
