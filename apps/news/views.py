from rest_framework.views import APIView
from rest_framework import status
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response
from rest_framework.generics import ListAPIView
from rest_framework.parsers import JSONParser, MultiPartParser
from django.shortcuts import get_object_or_404
from PIL import Image
from uuid import uuid4

# Models
from .models import New

# Serializers
from .api.serializers import NewSerializer

class NewsView(ListAPIView):
    serializer_class = NewSerializer

    def get_queryset(self):
        return New.objects.filter(condominie=self.request.user.condominie)

class UpdateNew(APIView):
    permission_classes = (IsAdminUser,)

    def put(self, request):
        if not 'id' in request.data:
            return Response({'message': 'el id de la noticia es necesaria'}, status=status.HTTP_400_BAD_REQUEST)

        new = get_object_or_404(New, id=request.data['id'])
        allow_update = ['description', 'title', 'status']

        for key in request.data:
            if key in allow_update:
                setattr(new, key, request.data[key])
        new.save()
        new_serializer = NewSerializer(new)

        return Response(new_serializer.data)

        

class CreateNew(APIView):

    permission_classes = (IsAdminUser,)
    parser_classes = (JSONParser, MultiPartParser)

    def post(self, request):
        if not 'title' in request.data or not 'description' in request.data:
            return Response({'message': 'Todos los campos son requeridos: title, description'}, status=status.HTTP_400_BAD_REQUEST)

        image = None
        title = request.data['title']
        description = request.data['description']

        if 'image' in request.data:
            new_image = request.data['image']

            try:
                img = Image.open(new_image)
                img.verify()
            except:
                return Response({'message': 'Este tipo de documento no se permite'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            
            new_image_for_extension = new_image.name.split('.')
            new_image.name = '{}.{}'.format(uuid4(), new_image_for_extension[len(new_image_for_extension)-1])

            image = new_image

        new = New(title=title, description=description, condominie=request.user.condominie)
        if image:
            new.image = image
        
        new.save()

        new_serializer = NewSerializer(new)
        return Response(new_serializer.data, status=status.HTTP_201_CREATED)



        
            
            
       
        
