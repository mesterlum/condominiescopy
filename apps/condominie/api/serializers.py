from rest_framework import serializers

# Models
from ..models import Condominie

class CondominieSerializer(serializers.ModelSerializer):
    class Meta:
        model = Condominie
        fields = ('name', 'address', 'status',)