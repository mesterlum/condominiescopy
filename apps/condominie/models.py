from django.db import models

# Models

class Condominie(models.Model):

    STATUS_CHOICES =(
        ('A', 'Active'),
        ('I', 'Inactive')
    )
    name = models.CharField(max_length=100, blank=False, null=False)
    address = models.CharField(max_length=150, blank=False, null=False)
    status = models.CharField(max_length=1, choices=STATUS_CHOICES, default=STATUS_CHOICES[0][0])
    date_register = models.DateTimeField(auto_now_add=True)
    

