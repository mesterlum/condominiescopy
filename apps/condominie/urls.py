from django.urls import path

from .views import CondominieView, UpdateInfoCondominieView, GetInfoCondominieView

urlpatterns = [
    path('', CondominieView.as_view()),
    path('update', UpdateInfoCondominieView.as_view()),
    path('info', GetInfoCondominieView.as_view()),

]