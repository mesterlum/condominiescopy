from django.apps import AppConfig


class CondominieConfig(AppConfig):
    name = 'condominie'
