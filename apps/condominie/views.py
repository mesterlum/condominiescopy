from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import AllowAny, IsAdminUser
from django.shortcuts import get_object_or_404

# Models
from .models import Condominie
from apps.users.models import Users # TMP
# Serializers
from .api.serializers import CondominieSerializer
class CondominieView(APIView):
    
    permission_classes = (AllowAny,)

    def post(self, request):
        if not 'name' in request.data or not 'address' in request.data:
            return Response({'message': 'Tiene que enviar todos los campos, name, last_name, address'}, 
                status=status.HTTP_400_BAD_REQUEST)

        name = request.data['name']
        address = request.data['address']
        condominie = Condominie(name=name, address=address)
        condominie.save()

        condominie_serializer = CondominieSerializer(condominie)
        
        return Response(condominie_serializer.data, status=status.HTTP_201_CREATED)

class UpdateInfoCondominieView(APIView):

    permission_classes = (IsAdminUser,)

    def put(self, request):
        condominie = get_object_or_404(Condominie, id=request.user.condominie)

        allowed_fields = ['name', 'address']

        for key in request.data:
            if key in allowed_fields:
                setattr(condominie, key, request.data[key])

        condominie.save()

        condominie_serializer = CondominieSerializer(condominie)
        
        return Response(condominie_serializer.data)

class GetInfoCondominieView(APIView):

    permission_classes = (IsAdminUser,)

    def get(self, request):

        condominie = get_object_or_404(Condominie, id=request.user.condominie)
        condominie_serializer = CondominieSerializer(condominie)

        return Response(condominie_serializer.data)
