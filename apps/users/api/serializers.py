from rest_framework import serializers
from ..models import Users

from apps.condominie.api.serializers import CondominieSerializer


class UsersSerializer(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = ('id', 'email', 'first_name', 'last_name', 'phone',)

class UserSerializerWithCondominie(serializers.ModelSerializer):
    condominie = CondominieSerializer()
    class Meta:
        model = Users
        fields = ('id', 'email', 'first_name', 'last_name', 'phone', 'condominie')