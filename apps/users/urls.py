from django.urls import path

from .views import RegisterView, UserView, ChangePasswordView, DetailUser, UsersWitOutDepartament

# JWT auth
from rest_framework_jwt.views import obtain_jwt_token, verify_jwt_token, refresh_jwt_token

urlpatterns = [
    path('register', RegisterView.as_view()),
    path('login', obtain_jwt_token),
    path('verify-token', verify_jwt_token),
    path('refresh-token', refresh_jwt_token),
    path('', UserView.as_view()),
    path('change-password', ChangePasswordView.as_view()),
    path('detail', DetailUser.as_view()),
    path('without-departament', UsersWitOutDepartament.as_view()),
]