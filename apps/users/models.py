from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser
from .managers import UserManager

# extern models
from apps.condominie.models import Condominie

class Users(AbstractBaseUser):
    email = models.EmailField(unique=True)
    first_name = models.CharField(max_length=30, blank=False)
    last_name = models.CharField(max_length=30, blank=True)
    phone = models.CharField(max_length=15, blank=False)
    date_joined = models.DateTimeField(auto_now_add=True)
    date_update = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    condominie = models.ForeignKey(Condominie, on_delete=models.CASCADE)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    def __str__(self):
        return self.email

    def get_short_name(self):
        return self.first_name
    
    def get_full_name(self):
        return '{} {}'.format(self.first_name, self.last_name)

    