from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.permissions import IsAdminUser
from rest_framework.response import Response
from rest_framework import status

# Utils
from .utils import generate_password

# Models
from .models import Users
from apps.departament.models import DepartamentUser

# Serializer
from .api.serializers import UsersSerializer, UserSerializerWithCondominie

#This class is only for register users
class RegisterView(APIView):
    permission_classes = (IsAdminUser,)

    def post(self, request):
        if not 'email' in request.data or not 'first_name' in request.data or not 'last_name' in request.data or not 'phone' in request.data:
            return Response({'message': 'Tiene que enviar todos los campos, email, first_name, last_name, phone'}, 
                status=status.HTTP_400_BAD_REQUEST)

        

        email = request.data['email']

        try:
            Users.objects.get(email=email)
            return Response({'message': 'Este email ya esta registrado'}, status=status.HTTP_409_CONFLICT)
        except Users.DoesNotExist:
            pass

        password = generate_password()
        first_name = request.data['first_name']
        last_name = request.data['last_name']
        phone = request.data['phone']

        user = Users.objects.create_user(email, password)
        user.first_name = first_name
        user.last_name = last_name
        user.phone = phone
        user.is_active = True # Automatic active user
        user.condominie = request.user.condominie
        user.save()

        user_serializer = UsersSerializer(user)
        context = {'password': password}
        context.update(user_serializer.data)
        return Response(context, status=status.HTTP_201_CREATED)


class UserView(APIView):

    # Update User

    def put(self, request):
        
        user = get_object_or_404(Users, id=request.user.id)
        for key in request.data:
            setattr(user, key, request.data[key])

        user.save()

        user_serializer = UsersSerializer(user)

        return Response(user_serializer.data)

class ChangePasswordView(APIView):

    # Update password

    def put(self, request):

        if not 'new_password' in request.data:
            return Response({'message': 'Todos los parametros son requeridos: new_password'}, status=status.HTTP_400_BAD_REQUEST)

        

        user = get_object_or_404(Users, id=request.user.id)

        new_password = request.data['new_password']

        user.set_password(new_password)
        user.save()
        return Response({'message': 'Password cambiada'})

class DetailUser(APIView):

    def get(self, request):
        user_serializer = UserSerializerWithCondominie(request.user)
        return Response(user_serializer.data)


class UsersWitOutDepartament(APIView):

    permission_classes = (IsAdminUser,)

    def get(self, request):
        users = Users.objects.filter(condominie=request.user.condominie)

        context = []
        for user in users:
            try:
                DepartamentUser.objects.get(user=user)
                # ...
            except DepartamentUser.DoesNotExist:
                user_serializer = UsersSerializer(user)
                context.append(user_serializer.data)

        return Response(context)
