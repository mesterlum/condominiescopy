from django.urls import path

from . import views

urlpatterns = [
    path('create', views.CreateReportView.as_view()),
    path('filter', views.FilterReportsView.as_view()),
    path('change-status', views.ChangeStatusView.as_view()),
    path('check', views.ApplyCheckReportView.as_view()),
    path('update', views.UpdateReport.as_view()),
    path('user', views.ListReportsOfTheClientsView.as_view()),
    path('change-pending', views.ChangeAsPendingReport.as_view()),
    path('message', views.MessageForReportView.as_view()),
]