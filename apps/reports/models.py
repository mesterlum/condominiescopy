from django.db import models

# Models
from apps.condominie.models import Condominie
from apps.departament.models import Departament


class Report(models.Model):

    STATUS_CHOICES = (
        ('U', 'Unsolved'),
        ('P', 'Pending'),
        ('R', 'Resolved'),
    )


    description = models.CharField(max_length=250, null=False)
    condominie = models.ForeignKey(Condominie, on_delete=models.CASCADE)
    departament_report = models.ForeignKey(Departament, on_delete=models.CASCADE)
    status = models.CharField(max_length=1, choices=STATUS_CHOICES, default=STATUS_CHOICES[0][0])
    check = models.BooleanField(default=False)
    date = models.DateTimeField(auto_now_add=True)
    message = models.TextField(max_length=600, null=True)
