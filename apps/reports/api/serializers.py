from rest_framework import serializers

# Models
from ..models import Report

# Serializers
from apps.departament.api.serializers import DepartamentSerializer

class ReportSerializer(serializers.ModelSerializer):
    class Meta:
        model = Report
        fields = ('id', 'description', 'status', 'date', 'check', 'message',)

class ReportWithDepartamentSerializer(serializers.ModelSerializer):
    
    departament = DepartamentSerializer(source='departament_report')

    class Meta:
        model = Report
        fields = ('id', 'description', 'status', 'date', 'check', 'departament', 'message',)