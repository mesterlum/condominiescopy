from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from django.shortcuts import get_object_or_404
from rest_framework.generics import ListAPIView

# Models
from .models import Report
from apps.departament.models import DepartamentUser

# Serializers
from .api.serializers import ReportSerializer, ReportWithDepartamentSerializer


class CreateReportView(APIView):

    def post(self, request):

        if not 'description' in request.data:
            return Response({'message': 'Todos los parametros son requeridos: description'}, status=status.HTTP_400_BAD_REQUEST)
            
        departament = get_object_or_404(DepartamentUser, user=request.user)
        description = request.data['description']

        report = Report(description=description, condominie=request.user.condominie, departament_report=departament.departament)
        report.save()

        report_serializer = ReportSerializer(report)

        return Response(report_serializer.data, status=status.HTTP_201_CREATED)


class FilterReportsView(APIView):
    
    permission_classes = (IsAdminUser,)

    def get(self, request):
        
        status = ''
        if not 'status' in request.GET:
            status = 'U'
        else:
            status = request.GET['status']

        reports = Report.objects.filter(condominie=request.user.condominie, status=status)
        reports_serializer = ReportWithDepartamentSerializer(reports, many=True)
            
        return Response(reports_serializer.data)

class ChangeStatusView(APIView):

    permission_classes = (IsAdminUser,)

    def put(self, request):
        if not 'pk_report' in request.data or not 'new_status' in request.data:
            return Response({'message': 'Todos los parametros son requeridos: pk_report, new_status'}, status=status.HTTP_400_BAD_REQUEST)

        new_status = request.data['new_status']

        if new_status != 'U' and new_status != 'P' and new_status != 'R':
            return Response({'message': 'new_status debe de ser U, P, R'}, status=status.HTTP_404_NOT_FOUND)

        pk_report = int(request.data['pk_report'])

        report =  get_object_or_404(Report, id=pk_report, condominie=request.user.condominie)
        report.status = new_status
        report.save()

        report_serializer = ReportSerializer(report)
        return Response(report_serializer.data)

class UpdateReport(APIView):
    
    def put(self, request):
        if not 'id' in request.data:
            return Response({'message': 'El id es requerido'}, status=status.HTTP_400_BAD_REQUEST)
        
        report = get_object_or_404(Report, id=request.data['id'], condominie=request.user.condominie)

        allowed_fields = ['description']

        for key in request.data:
            if key in allowed_fields:
                setattr(report, key, request.data[key])

        report.save()

        report_serializer = ReportSerializer(report)

        return Response(report_serializer.data)

class ApplyCheckReportView(APIView):

    permission_classes = (IsAdminUser,)

    def put(self, request):
        if not 'id' in request.data:
            return Response({'message': 'El id es necesario'}, status=status.HTTP_400_BAD_REQUEST)

        report = get_object_or_404(Report, id=request.data['id'], condominie=request.user.condominie)

        report.check = True
        report.save()

        report_serializer = ReportSerializer(report)

        return Response(report_serializer.data)


class ListReportsOfTheClientsView(ListAPIView):
    
    serializer_class = ReportSerializer

    def get_queryset(self):
        departament = get_object_or_404(DepartamentUser, user=self.request.user)
        return Report.objects.filter(departament_report=departament.departament)

class ChangeAsPendingReport(APIView):

    permission_classes = (IsAdminUser,)

    def put(self, request):

        if not 'id' in request.data:
            return Response({'message': 'El id es requerido'}, status=status.HTTP_400_BAD_REQUEST)
        
        report = get_object_or_404(Report, id=request.data['id'], condominie=request.user.condominie)

        report.status = 'P'

        report.save()

        report_serializer = ReportSerializer(report)

        return Response(report_serializer.data)

class MessageForReportView(APIView):

    permission_classes = (IsAdminUser,)

    def put(self, request):
        if not 'id' in request.data or not 'message' in request.data:
            return Response({'message': 'Todos los parametros son requeridos: id, message'}, status=status.HTTP_400_BAD_REQUEST)

        report = get_object_or_404(Report, id=request.data['id'], condominie=request.user.condominie)

        report.message = request.data['message']
        report.save()

        return Response({'message': 'Ok!'})


        




