### Expenses

---
###### Register expense
* **POST** /api/v1/expenses/

payload:

```json
{
    "concept": String,
    "description": String,
    "price": Int
}
```

Responses:

**400** - All params are requerid

return:

```json
    {
        "message": String
    }
```

**201** - Expense created

return:

```json
    {
        "id": Int,
        "concept": String,
        "description": String,
        "price": Int,
        "status": String,   ('PE' = Pending, 'PA' = Paid Out)
        "created_at": Date
    }
```

###### Get all expenses
* **GET** /api/v1/expenses/

return:

```json
[
    {
        "id": Int,
        "concept": String,
        "description": String,
        "price": Int,
        "status": String,   ('PE' = Pending, 'PA' = Paid Out)
        "created_at": Date
    }
]
```

###### Update expense
* **PUT** /api/v1/expenses/

payload:

```json
    {
        "id": Int,
        "concept": String,
        "description": String,
        "price": Int,
        "status": String    ('PE' = Pending, 'PA' = Paid Out)
    }
```

Responses:

**400** - All params are requerid

return:

```json
    {
        "message": String
    }
```

**404** - Expense not found

return:

```json
    {
        "detail": String
    }
```

**201** - Expense update

return:

```json
    {
        "id": Int,
        "concept": String,
        "description": String,
        "price": Int,
        "status": String,   ('PE' = Pending, 'PA' = Paid Out)
        "created_at": Date
    }
```

###### Filter expense
* **PUT** /api/v1/expenses/filter **?status=String**

> If not defined status, the API response with all records

> status = ('PE' = Pending, 'PA' = Paid Out)

* Example
```javascript
    // Correct
    fetch('/api/v1/expenses/filter?status=PE')
    fetch('/api/v1/expenses/filter')
```

Response:

**200** - List of the expenses

```json
[
    {
        "id": Int,
        "concept": String,
        "description": String,
        "price": Int,
        "status": String,   ('PE' = Pending, 'PA' = Paid Out)
        "created_at": Date
    }
]
```
