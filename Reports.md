### Reports

---
###### Register report
* **POST** /api/v1/reports/create

Payload:

```json
    {
        "description": String
    }
```

Reponse:

**400** -All parameters are required

return:

```json
    {
        "message": String
    }
```

**404** - The user no contain departament

return:

```json
    {
        "detail": String
    }
```

**201** - Report register

return:

```json
    {   "id": Int,
        "description": String,
        "status": String,
        "check": boolean,
        "date": Date
    }
```
---
###### Filter report
* **GET** /api/v1/reports/filter **?status=String**

> status acepted "U" = Unsolved, "P" = Partial, "R" = Resolved

>>If exists "status" in filter URL param take the values, if not exists take default value "U"

* Example
```javascript
    // Correct
    fetch('/api/v1/reports/filter?status=U')
    fetch('/api/v1/reports/filter')

    //Incorrect
    fetch('/api/v1/reports/filter?status=AB')


```
**200** - Response with all data filtering
Return:

```json
    [
        {   
            "id": Int,
            "description": String,
            "status": String,
            "check": boolean,
            "date": Date,
            "departament":{
                "number": String,
                "status": String,
                "date": Date
            }
        }
    ]
```
---

###### Change Status report
---
* **PUT** /api/v1/reports/change-status

payload:

```json
    {
        "new_status": String,
        "pk_report": Int
    }
```

Responses:

**400** -All parameters are required

return:

```json
    {
        "message": String
    }
```

**404** - The param 'new_status' only accept U, P, R

return:

```json
    {
        "message": String
    }
```

**200** - Change status

return:

```json
    {
        "id": Int,
        "description": String,
        "status": String,
        "check": boolean,
        "date": Date        
    }
```

---


###### apply check
---
* **PUT** /api/v1/reports/check

payload:

```json
    {
        "id": Int
    }
```

Responses:

**400** -All parameters are required

return:

```json
    {
        "message": String
    }
```

**404** - Report not found

return:

```json
    {
        "detail": String
    }
```

**200** - check applied

return:

```json
    {
        "id": Int,
        "description": String,
        "status": String,
        "check": boolean,
        "date": Date        
    }
```

###### Update report
* **PUT** /api/v1/reports/update

payload:

```json
    {
        "id": Int,
        "description": String   (Optional)
    }
```

Reponse:

**400** -All parameters are required

return:

```json
    {
        "message": String
    }
```

**404** - Event not found

return:

```json
    {
        "detail": String
    }
```

**200**

return:

```json
    {
        "id": Int,
        "description": String,
        "status": String,
        "check": boolean,
        "date": Date        
    }
```

###### Get reports of the users
* **PUT** /api/v1/reports/user

response:

**404** - Departament not found

```json
{
    "detail": String
}
```

**200**

return:

```json
[
    {
        "id": Int,
        "description": String,
        "status": String,
        "check": boolean,
        "date": Date        
    }
]
```

###### Change status as pending
* **PUT** /api/v1/reports/change-pending

payload:

```json
    {
        "id": Int
    }
```

Reponse:

**400** -All parameters are required

return:

```json
    {
        "message": String
    }
```

**404** - Report not found

return:

```json
    {
        "detail": String
    }
```

**200**

return:

```json
    {
        "id": Int,
        "description": String,
        "status": String,
        "check": boolean,
        "date": Date        
    }
```


###### Messages for report
* **PUT** /api/v1/reports/message

payload:

```json
    {
        "id": Int,
        "message": String
    }
```

Reponse:

**400** -All parameters are required

return:

```json
    {
        "message": String
    }
```

**404** - Report not found

return:

```json
    {
        "detail": String
    }
```

**200** - Message acepted

return:

```json
    {
        "message": String
    }
```

